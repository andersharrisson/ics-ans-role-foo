# ics-ans-role-foo

Ansible role to install foo.

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-foo
```

## License

BSD 2-clause
